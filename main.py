import typer


app = typer.Typer()


def get_inputs():
    try:
        while True:
            yield input()
    except EOFError:
        pass


def apply_from_to(value, apply_function, start, stop):
    return value[:start] + apply_function(value[start:stop]) + (value[stop:] if stop is not None else "")


@app.command()
def upper(start: int = typer.Option(0, '--from', '-f'), stop: int = typer.Option(None, '--to', '-t')):
    for value in get_inputs():
        print(apply_from_to(value, str.upper, start, stop))


@app.command()
def lower(start: int = typer.Option(0, '--from', '-f'), stop: int = typer.Option(None, '--to', '-t')):
    for value in get_inputs():
        print(apply_from_to(value, str.lower, start, stop))


if __name__ == "__main__":
    app()
